import java.util.stream.IntStream;

public class Main {

    // un super sudoku
    private final int[][] grille = new int[][] {
            {6, 0, 0, 0, 5, 3, 0, 0, 0},
            {0, 2, 9, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 7, 0, 0, 1, 0, 8},
            {0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 2, 0, 0, 8, 0, 0},
            {3, 6, 0, 0, 0, 5, 0, 9, 7},
            {7, 3, 1, 8, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 4, 0, 0},
            {0, 0, 4, 0, 9, 0, 6, 0, 0},
    };

    public Main() {
        afficher();
        estValide( 0);
        System.out.println("---------------------------------");
        afficher();
    }

    /**
     * Permets de savoir si k est absent dans une ligne
     *
     * @param k
     * @param i
     * @return
     */
    public boolean absentSurLigne (int k, int i) {
        return IntStream.range(0, 9).noneMatch(value -> grille[i][value] == k);
    }

    /**
     * Permets de savoir si k est absent dans une colonne
     *
     * @param k
     * @param j
     * @return
     */
    public boolean absentSurColonne (int k, int j) {
        return IntStream.range(0, 9).noneMatch(value -> grille[value][j] == k);
    }

    /**
     * Permets de savoir si k n'est pas dans un carre
     *
     * @param k
     * @param i
     * @param j
     * @return Boolean, si k est absent dans le carre
     */
    public boolean absentCarre(int k, int i, int j) {
        int x = i-(i%3);
        int y = j-(j%3);

        return IntStream.range(x, x+3).noneMatch(ii -> IntStream.range(y, y+3).anyMatch(jj -> grille[ii][jj] == k));
    }

    /**
     * Permets de résoudre un sudoku
     *
     * @param position, Position dans l'exploration sudoku
     * @return Boolean, si la valeur choisis est la bonne
     */
    public boolean estValide (int position) { // position est plus utile qu'un objet Coordinate pour le back tracking
        if (position == 9*9) return true; // FINIS !!!

        int i = position/9;
        int j = position%9;

        // On continue
        if (grille[i][j] != 0) return estValide(position+1);

        // On check partout
        for (int k=1; k <= 9; k++) {
            if (absentSurLigne(k, i) && absentSurColonne(k, j) && absentCarre(k, i, j)) {
                grille[i][j] = k;

                // On vérif que c'est toujours bon avec le prochain (cf. Backtracking), grace à la récursion si un n'est pas bon tout le chemin s'écroule
                if (estValide(position+1)) return true;
            }
        }

        //On efface nos tests...
        grille[i][j] = 0;
        return false;
    }

    /**
     * Permet d'afficher le sudoku
     */
    public void afficher() {
        IntStream.range(0, 9).forEach(i -> {
            if(i%3 == 0) {
                System.out.println("  ------------------------");
            }
            IntStream.range(0, 9).forEach(y -> {
                int n = grille[i][y];
                System.out.print((y)%3 == 0 ? " | " + n : " " + n);
            });

            System.out.println(" |");
        });
    }

    public static void main(String[] args) {
        new Main();
    }
}
